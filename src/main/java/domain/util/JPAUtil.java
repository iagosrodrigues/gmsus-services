package domain.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	
	private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory("gmsus");
	
	public static EntityManager createEntityManager() {
		return factory.createEntityManager();
	}
}
