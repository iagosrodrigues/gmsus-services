package domain.persistence;

import java.util.List;

import javax.persistence.EntityManager;

import domain.model.Prescricao;
import domain.model.Usuario;

public class PrescricaoDAO extends AbstractDAO<Prescricao> {

	public PrescricaoDAO(EntityManager entityManager) {
		super(entityManager, Prescricao.class);
	}

	public List<Prescricao> getPrescricoesByUsuario(Usuario usuario) {
		return getEntityManager()
				.createQuery("select P from prescricao P where P.usuario = :usuario", Prescricao.class)
				.setParameter("usuario", usuario).getResultList();
	}
}
