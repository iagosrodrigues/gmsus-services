package domain.persistence;

import javax.persistence.EntityManager;

public class AbstractDAO<E> {

	private EntityManager entityManager;
	private Class<E> persistedClass;
	
	public AbstractDAO(EntityManager entityManager, Class<E> persistedClass) {
		this.entityManager = entityManager;
		this.persistedClass = persistedClass;
	}
	
	public E getById(Long id) {
		return entityManager.find(persistedClass, id);
	}

	public boolean save(E entity) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(entity);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean edit(E entity) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(entity);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean remove(Long id) {
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(getById(id));
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
}
