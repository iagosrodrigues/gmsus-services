package domain.persistence;

import javax.persistence.EntityManager;

import domain.model.Medicamento;

public class MedicamentoDAO extends AbstractDAO<Medicamento>{

	public MedicamentoDAO(EntityManager entityManager) {
		super(entityManager, Medicamento.class);
	}

}
