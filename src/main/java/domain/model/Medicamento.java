package domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Medicamento {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String termo;
	
	@Column
	private String tarja;
	
	@Column
	private Integer quantidadeEstoque;

	public Medicamento() {
		// TODO Auto-generated constructor stub
	}
	
	public Medicamento(String termo, String tarja, Integer quantidadeEstoque) {
		super();
		this.termo = termo;
		this.tarja = tarja;
		this.quantidadeEstoque = quantidadeEstoque;
	}

	public String getTermo() {
		return termo;
	}

	public void setTermo(String termo) {
		this.termo = termo;
	}

	public String getTarja() {
		return tarja;
	}

	public void setTarja(String tarja) {
		this.tarja = tarja;
	}

	public Integer getQuantidadeEstoque() {
		return quantidadeEstoque;
	}

	public void setQuantidadeEstoque(Integer quantidadeEstoque) {
		this.quantidadeEstoque = quantidadeEstoque;
	}

	public Long getId() {
		return id;
	}
}
