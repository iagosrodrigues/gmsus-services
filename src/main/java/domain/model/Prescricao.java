package domain.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Prescricao {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String urlImagem;
	
	@Column
	private LocalDateTime dataPrescricao;
	
	@Column
	private Integer quantidade;
	
	@ManyToOne
	private Medicamento medicamento;
	
	@ManyToOne
	private Usuario usuario;
	
	public Prescricao() {
		// TODO Auto-generated constructor stub
	}
	
	public Prescricao(String urlImagem, LocalDateTime dataPrescricao, Integer quantidade, Medicamento medicamento,
			Usuario usuario) {
		super();
		this.urlImagem = urlImagem;
		this.dataPrescricao = dataPrescricao;
		this.quantidade = quantidade;
		this.medicamento = medicamento;
		this.usuario = usuario;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

	public LocalDateTime getDataPrescricao() {
		return dataPrescricao;
	}

	public void setDataPrescricao(LocalDateTime dataPrescricao) {
		this.dataPrescricao = dataPrescricao;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Medicamento getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Long getId() {
		return id;
	}
}
