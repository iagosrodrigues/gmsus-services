package domain.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Unidade {

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true)
	private String nome;

	@OneToOne(cascade = CascadeType.PERSIST)
	private Endereco endereco;

	@OneToMany(cascade = CascadeType.PERSIST)
	private List<Contato> contatos;

	@ManyToMany
	private List<Medicamento> medicamentos;

	public Unidade(String nome, Endereco endereco, Contato contato) {
		this.nome=nome;
		this.endereco=endereco;
		contatos = new ArrayList<>();
		contatos.add(contato);
	}
	
	public Unidade(String nome, Endereco endereco, List<Contato> contatos) {
		super();
		this.nome = nome;
		this.endereco = endereco;
		this.contatos = contatos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}

	public List<Medicamento> getMedicamentos() {
		return medicamentos;
	}

	public void setMedicamentos(List<Medicamento> medicamentos) {
		this.medicamentos = medicamentos;
	}

	public Long getId() {
		return id;
	}
}
