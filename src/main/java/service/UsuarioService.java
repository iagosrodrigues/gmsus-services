package service;

import java.time.LocalDateTime;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import domain.model.Contato;
import domain.model.Endereco;
import domain.model.Usuario;
import domain.persistence.UsuarioDAO;
import domain.util.JPAUtil;

@Path("usuarios")
public class UsuarioService {

	private UsuarioDAO dao = new UsuarioDAO(JPAUtil.createEntityManager());

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@QueryParam(value = "nome") String nome) {
		List<Usuario> usuarios = null;
		try {
			if (nome == null || nome.trim().isEmpty())
				usuarios = dao.getAll();
			else
				usuarios = dao.getAllByName(nome);
		} catch (Exception e) {
			System.out.println(usuarios);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.status(Response.Status.OK).entity(usuarios).build();
	}

	@GET
	@Path("/{id}")
	public Response getUser(@PathParam("id") Long id) {
		Usuario usuario;
		try {
			usuario = dao.getById(id);
		} catch (Exception e) {
			// TODO: Tratar acesso negado e retornar status adequado
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.status(Response.Status.OK).entity(usuario).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response save(String json) {
		Usuario novoUsuario = new Gson().fromJson(json, Usuario.class);
		try {
			// TODO: Validar usuario antes de persistir
			dao.save(novoUsuario);
		} catch (Exception e) {
			// TODO: Retornar status adequado ao erro
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Response.Status.CREATED).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response edit(String json) {
		Usuario usuario = new Gson().fromJson(json, Usuario.class);
		try {
			dao.edit(usuario);
		} catch (Exception e) {
			// TODO: Retornar status adequado ao erro
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.status(Response.Status.OK).build();
	}

	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		
		try {
			dao.remove(id);
		} catch (Exception e) {
			// TODO: Retornar status adequado ao erro
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.status(Response.Status.OK).build();
	}

	/*
	 * URLs para auxiliar o desenvolvimento
	 */
	@GET
	@Path("status")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getStatus() {
		return Response.status(Response.Status.OK).entity("Funcionando").build();
	}

	@GET
	@Path("sample")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSample() {
		return Response.status(Response.Status.OK)
				.entity(new Usuario("nome", "12345678900", "1123456789", LocalDateTime.now(),
						new Contato("exemplo@email.com", "88888888"), new Endereco("bairro", "rua", "100", "61840095")))
				.build();
	}
}
