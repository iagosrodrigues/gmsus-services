package app;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import domain.model.Contato;
import domain.model.Endereco;
import domain.model.Medicamento;
import domain.model.Prescricao;
import domain.model.Unidade;
import domain.model.Usuario;
import domain.persistence.MedicamentoDAO;
import domain.persistence.PrescricaoDAO;
import domain.persistence.UnidadeDAO;
import domain.persistence.UsuarioDAO;
import domain.util.JPAUtil;

public class Test {
	public static void main(String[] args) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("gmsus");

//		Contato contatoUsuario = new Contato("contatoUsuario@email.com", "99999999");
//		Contato contato1Unidade = new Contato("contato1Unidade@gmal.com", "88888888");
//		Contato contato2Unidade = new Contato("contato1Unidade@gmal.com", "77777777");
//		List<Contato> contatosUnidade = new ArrayList<>();
//		contatosUnidade.add(contato1Unidade);
//		contatosUnidade.add(contato2Unidade);
//		
//		Endereco enderecoUsuario = new Endereco("Centro", "Rua norte", "99", "60000000");
//		Endereco enderecoUnidade = new Endereco("Centro", "Rua sul", "100", "70000000");
//
//		Usuario usuario = new Usuario("isaac", "61067292390", "1111111", LocalDateTime.now(), contatoUsuario, enderecoUsuario);
//		
//		Unidade unidade = new Unidade("Farmacia central", enderecoUnidade, contatosUnidade); 
//		Medicamento medicamento = new Medicamento("Gardenal", "Preta", 10);
//		
//		UsuarioDAO usuarioDao = new UsuarioDAO(factory.createEntityManager());
//		UnidadeDAO unidadeDAO = new UnidadeDAO(factory.createEntityManager());
//		MedicamentoDAO medicamentoDAO = new MedicamentoDAO(factory.createEntityManager());
//		PrescricaoDAO prescricaoDao = new PrescricaoDAO(factory.createEntityManager());
//
//		usuarioDao.save(usuario);
//		unidadeDAO.save(unidade);
//		medicamentoDAO.save(medicamento);
//		prescricaoDao.save(new Prescricao("caminho/para/a/imagem", LocalDateTime.now(), 17, medicamento, usuario));

		UsuarioDAO dao = new UsuarioDAO(JPAUtil.createEntityManager());
		System.out.println(dao.getAll());
		factory.close();
	}
}
